package com.recipebookGWT.main.client.registration;

import com.google.gwt.user.client.ui.*;


public class RegistrationView extends Composite {


    private final DecoratorPanel decoratorPanel = new DecoratorPanel();
    private final Label nameLabel = new Label("Name:");
    private final TextBox nameBox = new TextBox();
    private final Label passwdLabel = new Label("Password:");
    private final PasswordTextBox passwd = new PasswordTextBox();
    private final Label retypeLabel = new Label("Retype Password:");
    private final PasswordTextBox retype = new PasswordTextBox();
    private final Button button = new Button("Submit");
    private final DialogBox dlg=new DialogBox();

    Grid grid = new Grid(4, 2);

    public RegistrationView() {

        grid.setWidget(0, 0, nameLabel);
        grid.setWidget(0, 1, nameBox);
        grid.setWidget(1, 0, passwdLabel);
        grid.setWidget(1, 1, passwd);
        grid.setWidget(2, 0, retypeLabel);
        grid.setWidget(2, 1, retype);
        grid.setWidget(3, 1, button);
        RootPanel.get().add(grid);
        decoratorPanel.add(grid);
        initWidget(decoratorPanel);


        button.addClickHandler(event->{
            new MyDialog().show();
        });
    }
    private  class MyDialog extends DialogBox {
        private final DecoratorPanel decoratorPanel = new DecoratorPanel();
        public MyDialog() {
            setStyleName("Dialog");


            // Set the dialog box's caption.
              setText("My First Dialog");

            setAnimationEnabled(true);

            // Enable glass background.
            setGlassEnabled(true);
            setWidget(decoratorPanel);
            // DialogBox is a SimplePanel, so you have to set its widget property to
            // whatever you want its contents to be.
            Button ok = new Button("OK");
            ok.addClickHandler(event -> MyDialog.this.hide());
            setWidget(ok);
        }
    }

}
