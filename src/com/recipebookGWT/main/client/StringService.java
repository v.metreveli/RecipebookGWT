package com.recipebookGWT.main.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("check")
public interface StringService extends RemoteService {
    String checkString(String text);
}
