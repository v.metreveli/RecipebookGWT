package com.recipebookGWT.main.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.recipebookGWT.main.client.registration.RegistrationView;


public class RecipebookGWT implements EntryPoint {

    private final VerticalPanel vPanel = new VerticalPanel();
    private final HorizontalPanel hPanel = new HorizontalPanel();
    private final Button button = new Button("Click me");
    private final TextBox tbox = new TextBox();
    private final Label label = new Label();
    private long textlength;
    private StringServiceAsync stringService = GWT.create(StringService.class);

    public void onModuleLoad() {

        tbox.addKeyPressHandler(event -> {
            if (tbox.getText().equals("")) {
                textlength = 0;
            } else {
                textlength = tbox.getText().length() + 1;
            }
            label.setText(String.valueOf(textlength));
        });

        button.addClickHandler(event -> {
            stringService.checkString(tbox.getText(), new AsyncCallback<String>() {
                @Override
                public void onFailure(Throwable caught) {

                }

                @Override
                public void onSuccess(String result) {
                    label.setText(result);
                }
            });

            RegistrationView reg = new RegistrationView();
          //  vPanel.add(reg);
            RootPanel.get().clear();
            RootPanel.get().add(reg);

        });

        vPanel.add(button);
        vPanel.add(tbox);
        vPanel.add(label);

        RootPanel.get().add(vPanel);

    }


}
